<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    Protected $fillable = ['title', 'id'];
    protected $table = 'todolist';

    public function notes()
    {
		return $this->hasMany(Note::class);
    }

    public function addNote(Note $note)
    {
    	return $this->notes()->save($note);
    }

}
