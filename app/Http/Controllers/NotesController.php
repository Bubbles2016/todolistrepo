<?php

namespace App\Http\Controllers;

use App\Todo;
use App\Note;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    public function store(Request $request, Todo $todo)
    {
    	$todo->addNote(
    		new Note($request->all())
    	);	

    	return back();
    }
}
 