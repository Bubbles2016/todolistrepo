<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use App\http\Controllers\Controller;

class todolistController extends Controller
{
    public function index()
    {
    	$todolist = Todo::all();
    	return view ('todolist.index', compact('todolist'));
    }

    public function show(Todo $todo)
    {
    	
    	return view('todolist.show', compact('todo'));
    }

    public function store(Request $request, Todo $todo)
    {
        $todo = new Todo;
        $todo->title = $request->title; 
        $todo->save();
        return back();
    }

    public function edit(Todo $todo)
    {
        return view('todolist.edit', compact('todo'));
    }

    public function update(Request $request, Todo $todo)
    {
        $todo->update($request->all());

        return back();
    }

    public function delete(Todo $todo)
    {
        return view('todolist.destroy', compact('todo'));
        //$todo->delete();
        //$todolist = Todo::all();
        //return view ('todolist.index', compact('todolist'));
    
    }

    public function destroy(Todo $todo)
    {
        $todo->delete();
        $todolist = Todo::all();
        return view ('todolist.index', compact('todolist'));
    }

}
