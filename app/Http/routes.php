 <?php

Route::get('cards', 'cardsController@index');
Route::get('cards/{card}', 'cardsController@show');
Route::get('todolist', 'todolistController@index'); //to list all tasks
Route::get('todolist/{todo}', 'todolistController@show'); //to show certain task by id
Route::post('todolist', 'todolistController@store');
Route::get('todolist/{todo}/edit', 'todolistController@edit'); //to edit certain task by id
Route::patch('todolist/{todo}', 'todolistController@update'); //to save the update
Route::post('todolist/{todo}/notes', 'NotesController@store'); //to create a new task
Route::put('todolist/{todo}', 'todolistController@update'); //to save our update
Route::get('todolist/{todo}/delete','todolistController@delete'); //to display a view to delete a task
Route::delete('todolist/{todo}', 'todolistController@destroy'); //to actually delete the task
