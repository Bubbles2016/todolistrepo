@extends('master')

@section ('content')

	<h1>Edit The Task</h1>
	<hr>
	<form method="POST" action="">
		{{ csrf_field() }}
		<div>
			<textarea name="title">{{ $todo->title }}</textarea>
		</div>
		
		<div>	
			<button type="submit">Update Task</button>
		</div>	
	</form>	
@stop