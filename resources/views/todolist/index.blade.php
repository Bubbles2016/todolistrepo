@extends('master')

@section('content')

	<h1>Todo List</h1>

	@foreach ($todolist as $todo)
		
		<div>
			<a href="/todolist/{{ $todo->id }}">{{ $todo->title}} </a>
		</div>	

	@endforeach	
	<hr>
	<h3>Add a new Task</h3>
	<form method="POST" action="/todolist">
		{{ csrf_field() }}
		<div>
			<textarea name="title"></textarea>
		</div>
		
		<div>	
			<button type="submit">Add Task</button>
		</div>	
	</form>	


@stop
