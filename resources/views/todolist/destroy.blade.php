@extends('master')

@section ('content')

	<h1>Delete The Task</h1>
	<hr>
	<form method="POST" action="/todolist/{{ $todo->id }}">
		{{ method_field('delete') }}
		{{ csrf_field() }}
		<div>
			<textarea name="title">{{ $todo->title }}</textarea>
		</div>
		
		<div>	
			<button type="submit">Delete Task</button>
		</div>	
		
	</form>	

@stop